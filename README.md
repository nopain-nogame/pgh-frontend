# README #

Mockup: https://docs.google.com/document/d/1bHHFAh2z908Xy6ZOxmFQGxOh105TzYPVQAeliSUhKbY/edit

### Mockup der Web-Anwendung (Desktop) ###

#### Funtionen (aus Mockup): ####

##### 1.0 - Startbildschirm: #####
Der Startbildschirm ist rudimentär aufgebaut und soll lediglich die Optionen geben, als Spielleiter eine Session zu erstellen oder als Spieler eine ID einzugeben. Additive Informationen wie z.B. das Impressum, eine Datenschutzerklärung oder Informationen über die Anwendungen können im unteren rechten Bildschirmrand platziert werden.

##### 2.1 - Spielauswahl: #####
Wenn man eine Session erstellt, nimmt man die Funktion des Spielleiters ein und kann ein Spiel auswählen. Dabei soll je nach Auswahl ein erklärender Text Aufschluss über die Spieleranzahl, Spieldauer und weitere wichtige Kurzinformationen, sowie eine kurze Beschreibung des Spiels geben. Ebenfalls sollen konfigurierbare Spieloptionen aufgelistet werden. Ist man mit seiner Wahl zufrieden, kann man das Spiel auswählen und eröffnet die Session für andere.

##### 2.2 Spielleiter Session-Ansicht: #####
Die Lobby bieten als Spielleiter einen größeren Funktionsumfang. Zum einen gibt es die Auflistung beigetretener Mitspieler mit zuzüglichen Informationen und die Anzeige des eigenen Nicknames.
Zudem sind weitere Optionen, wie eine Möglichkeit der Kopie des Session-Links in die Zwischenablage, das Einstellen der Performance, das Setzen eines Passwortes oder das Beenden der Sitzung gebündelt anwählbar. Die Session ID ist permanent für potenzielle weitere Mitspieler sichtbar. Zudem soll ein Verweis zu grundsätzlichen Spieloptionen sichtbar sein. Ebenfalls kann die Session direkt beendet werden, wodurch der Spielleiter zum vorherigen Dialog (Spielauswahl) zurückkehrt

##### 2.2.1. Performance anpassen: #####
Die Anpassung der Performance ist Abhängig von den Möglichkeit der Einsparung von Ressourcen im jeweiligen Spiel. Hier können durch einfache Buttons Animationen und Effekte an und aus gestellt werden, sowie die Texturqualität dreistufig verringert oder erhöht werden.

##### 2.2.2. Passwort setzen: #####
Sobald das Passwort gesetzt oder verändert wird ist die Session nicht mehr öffentlich und kann nicht mehr nur durch die SessionID erreicht werden. 
-Passwort Generator -> Anforderung

##### 2.2.3 Lobby-Einsicht: #####
Grundsätzliche Daten über die Spielteilnehmer sollen gebündelt und sichtbar als Block platziert werden. Der Spielleiter soll auf einfache Art und Weise zum einen die Möglichkeit haben, Mitspieler aus der Session zu entfernen und zum anderen die “Bereit”-Anzeige umgehen und einen Spielstart erzwingen zu können. Der Nickname soll einfach zu ändern sein.

##### 2.3 Spieloptionen: #####
Der Spielleiter hat neben der Einsicht auf die grundsätzliche Spielauswahl auch das Recht das Spiel zu ändern. Die Auswahl ähnelt dem Menü bei der Erstellung der Session. Somit sind Informationen über das Spiel und individuelle Spieloptionen anwählbar. Allerdings sind durch die Informationen über die Anzahl der Spieler auch Warnungen bei Überschreitung der Spieleranzahl möglich. Spiele mit Warnungen können nicht ausgewählt werden. 

##### 3.1 Passworteingabe: #####
Hat man die Session-ID einer bestehenden Lobby eingegeben, kommt der Spieler entweder direkt in die Lobby oder muss bei einem Passwortschutz zunächst das richtige Passwort eingeben.

##### 3.2 Spielleiter Session-Ansicht: #####
Der Spieler hat wie der Spielleiter ebenfalls die Auswahl einen Session Link in die Zwischenablage zu kopieren, die Performance dem Gerät entsprechend anzupassen oder die Session zu beenden, wodurch er zum Startbildschirm zurückgeleitet wird.  Durch einen Klick auf “Bereit” signalisiert er seine Bereitschaft das Spiel zu starten. Die Änderung des Nicknames ist genauso einfach möglich wie beim SPielleiter. Zudem können Informationen zum Spiel eingesehen, aber nicht verändert werden.

##### 4.1. Spielfenster: #####
Sobald das Spiel beginnt, sollen die Funktionen der Anwendung in den Hintergrund treten. Durch einen Button sollen die Spielfunktionen für den Spieler und Spielleiter versteckt werden. Sie können so zu jeder Zeit im Spiel eingeblendet werden.