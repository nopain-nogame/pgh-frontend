import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { webSocket } from "rxjs/webSocket";
import { catchError, tap, switchAll, share } from 'rxjs/operators';
import { EMPTY, Subject } from 'rxjs';

export const WS_ENDPOINT = environment.wsEndpoint;

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  
  //@ts-ignore
  private socket$;
  private messagesSubject$ = new Subject();
  public messages$ = this.messagesSubject$.pipe(switchAll(), share(), catchError(e => {
    throw e;
  }));
  constructor() {
  }

  public connect(): void {
    
    if(!this.socket$ || this.socket$.closed) {
      console.log("[SocketService]: connecting...");
      this.socket$ = this.getNewWebSocket();
      const messages = this.socket$.pipe(
        tap({
          error: error => console.log(console.error),
        }), catchError(_ => EMPTY)
      );
      this.messagesSubject$.next(messages);
    }
  }

  private getNewWebSocket() {
    return webSocket({
      url: WS_ENDPOINT,
      protocol: 'echo-protocol',
      openObserver: {
        next: () => {
          console.log('[SocketService]: connection ok');
        }
      },
      closeObserver: {
        next: () => {
          console.log('[SocketService]: connection closed');
          this.close();
          this.connect();
        }
      }
    });
  }

  sendMessage(msg: any) {
    this.socket$.next(msg);
  }

  close() {
    this.socket$.complete();
  }
}
