import { Component, OnInit } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

import * as THREE from 'three';
import * as TWEEN from '@tweenjs/tween.js';
import {AnimationAction, AnimationClip, Group, Object3D} from 'three';
import {SocketService} from "../socket.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  private playerName;

  constructor(private socketService: SocketService, private router: Router) {
    if(this.getNicknameFromStorage() !== null){
      this.playerName = this.getNicknameFromStorage();
    }else{
      router.navigate(["/home"]);
    }
  }


  ngOnInit(): void { }
  private loadedPinTest: THREE.Object3D = new Object3D();
  ngAfterViewInit(): void {

    let testPin = new GLTFLoader();
    testPin.load('../assets/pictures/texturen/PinV2.glb', (gltf: GLTF) => {
      this.loadedPinTest = gltf.scene.clone(true);

      gltf.scene.scale.set(3, 3, 3);
      //console.log(gltf.animations[0]);


      //console.log(scene.getObjectByName(name));
      console.log("success");
      this.startThree();
    }, undefined, (error) => {
      console.error(error);
    });
  }


  private getNicknameFromStorage() {
    let nickname = localStorage.getItem("nickname");
    if(nickname !== null) {
      return nickname;
    }
    return null;
  }

  private gameFieldData: any[] = [];
  private playersHomeBases: any[] = [];

  sessionData$ = this.socketService.messages$.subscribe((msg: any) => {


    if (msg.command == 'sendToGameSession') {
      if(msg.action === 'roll') {
        if(this.playerName === msg.playerName){
          //Du selbst bist am Zug. Zeige das UI an um zu Würfeln
        }else{
          //Ein anderer Spieler ist am Zug
        }
      } else if(msg.action === 'playerDecideMovement') {

      } else if(msg.action === 'win') {

      }else if(msg.action === 'sendPlayersGameFieldUpdate'){
        //Hier werden alle Daten von der Spielmap empfangen.

        if(this.gameFieldData.length === 0){
          //Die Map wurde noch nicht inizialisiert. Lade alle Daten, die ankommen in die Map hinein
          console.log("Map wird initialisiert");
          this.gameFieldData = msg.gameFieldData;
          this.playersHomeBases = msg.playersHomeBases;

          console.log(this.playersHomeBases);
        }else{
          //es gibt bereits Map Informationen. Hier müssen die Unterschieder der vorherigen Map zur aktuellen festgestellt werden
          //Dinge wie Animationen etc. werden dann von hier aus gestartet.
        }
      }
    }
  })

  public drawGameFieldData(){
    for (let i = 0; i < this.gameFieldData.length; i++) {
      let mapPart = this.gameFieldData[i];
      if(mapPart.owner !== null){
        //Es befindet sich ein Spieler auf diesem Feld. Daher muss eine Figur erzeugt werden

      }
    }
  }

  public drawPlayerHomeBases(){
    for (let i = 0; i < this.playersHomeBases.length; i++) {
      let playerBasePart = this.playersHomeBases[i].playerBase;
      for (let j = 0; j < playerBasePart.length; j++) {
        let playerBasePartField = playerBasePart[j];
        if(playerBasePartField.owner !== null){
          //Es befindet sich ein Spieler auf diesem Feld. Daher muss eine Figur erzeugt werden

          let sceneObj = this.scene.getObjectByName("fieldhouse" + i + "Pos" + j);
          // @ts-ignore
          let position = [sceneObj.position.x, sceneObj.position.y, sceneObj.position.z];

          this.createPin("black", position, "player" + i + "Pin" + j);
        }
      }

    }
  }

  public pushRollDice(): void {
    let currID = localStorage.getItem("sessionID");
    let sessID: number;
    if(currID !== null) {
      sessID = +currID;
      let msgObject: any = {
        "command": "sendToGameSession",
        "sessionID": sessID,
        "action": "roll"
      }
      this.socketService.sendMessage(msgObject);
    }
  }

  public pushPlayerDecideMovement(playerGameField: number, gameFieldType: number): void {
    let currID = localStorage.getItem("sessionID");
    let sessID: number;
    if(currID !== null) {
      sessID = +currID;
      let msgObject: any = {
        "command": "sendToGameSession",
        "sessionID": sessID,
        "action": "playerDecideMovement",
        "playerGameField": playerGameField,
        "gameFieldType": gameFieldType
      }
      this.socketService.sendMessage(msgObject);
    }
  }

  public clock = new THREE.Clock();
  public pinAnimationMixer: THREE.AnimationMixer | undefined;
  public diceMixer: THREE.AnimationMixer | undefined;
  public action: THREE.AnimationAction | undefined;
  public pinAnimationClip: THREE.AnimationClip | undefined;
  //public pinAnimationClips: THREE.AnimationClip | undefined;
  public camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    1000
  );
  public renderer: THREE.WebGLRenderer | undefined;
  public controls: OrbitControls | undefined;
  public scene: THREE.Scene = new THREE.Scene();
  public raycaster = new THREE.Raycaster();
  public mouse = new THREE.Vector2();
  public allowClick = true;
  public INTERSECTED: THREE.Object3D | undefined;
  public selectedPinName = "";
  //let pinArray=[];


  public startThree() {

    console.log(this.loadedPinTest);

    //CAMERA
    this.camera.position.set(50, 100, -350);
    this.camera.lookAt(0, 0, 0);
    if (this.controls !== undefined) {
      this.controls.maxDistance = 400;
    }

    //BACKGROUND
    let filenames = ["px", "nx", "py", "ny", "pz", "nz"];
    let reflectionCube = new THREE.CubeTextureLoader().load(filenames.map(
      function (filename) {
        return "../assets/pictures/texturen/background/" + filename + ".jpg";
      }
    ));
    this.scene.background = reflectionCube;

    //LIGHT
    const hemisphereLight = new THREE.HemisphereLight(0xffffbb, 0x080820, 1)
    this.scene.add(hemisphereLight);
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    this.scene.add(directionalLight);

    //RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.shadowMap.enabled = true;
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setClearColor("rgb(60,60,60)"); //Hintergund des Rendeer
    let sceneWindow = document.getElementById("threejs");

    if (sceneWindow !== null) {
      sceneWindow.appendChild(this.renderer.domElement);
    }
    window.addEventListener('resize', this.onWindowResize, false);

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);


    this.buildGameBoard(0, 0, 0, 15);

    this.loadPins("red");
    this.loadPins("green");


    this.drawGameFieldData();
    this.drawPlayerHomeBases();


    //setTimeout(() => { this.movePinAt("greenPinA", "field0") }, 400);
    //setTimeout(() => { this.movePintAtAnimation("greenPinA", "field1") }, 800);
    //console.log(scene);
    setTimeout(() => { console.log(this.rollDice(2)) }, 1000);
    //setTimeout(()=>{console.log(scene.getChildByName("redPinA"))},100);
    //setTimeout(()=>{console.log(scene.getObjectByName("greenPinA"))},100);
    //setTimeout(()=>{movePintAtAnimation("greenPinA","field1")},3000);

    this.update();

  }

  //RESIZE WINDOW
  public onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    if (this.renderer !== undefined) {
      this.renderer.setSize(window.innerWidth, window.innerHeight);
    }
  }

  private lastIntersects: any;

  //RENDERLOOP
  public update() {

    let delta = this.clock.getDelta();
    if (this.pinAnimationMixer) this.pinAnimationMixer.update(delta);
    if (this.diceMixer) this.diceMixer.update(delta);
    TWEEN.update();
    if (this.renderer !== undefined) {
      this.renderer.render(this.scene, this.camera);
    }
    if (this.controls !== undefined) {
      this.controls.update();
    }
    requestAnimationFrame(() => this.update());

    const intersects = this.raycaster.intersectObjects(this.scene.children, true);

    if(this.lastIntersects !== intersects[0].object){
      this.lastIntersects = intersects[0].object;
      console.log(intersects[0].object);
    }

  }

  public onDocumentMouseClick(event: any) {
    event.preventDefault();
    console.log();
    this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    this.mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
    if (this.allowClick == true) {
      this.raycaster.setFromCamera(this.mouse, this.camera);
    }

  }


  //GERNERATE FIELD-CYLINDER-GEOMETRY
  public gameFieldGeometry(positionX: number, positionY: number, positionZ: number, name: string, color = "white") {
    let geometry = new THREE.CylinderGeometry(5, 5, 2, 32);
    let material = new THREE.MeshLambertMaterial({ color: this.whichColor(color) });
    let cylinder = new THREE.Mesh(geometry, material);
    cylinder.name = "field" + name.toString();
    //console.log(cylinder.name);
    cylinder.position.set(positionX, positionY, positionZ);
    this.scene.add(cylinder);
  }
  //CREATE ALL GAMEFIELDS
  public buildGameBoard(startX: number, startY: number, startZ: number, abstand: number) {
    //Spielfeld
    let fieldCounter = 0;
    let currentX = startX;
    let currentY = startY;
    let currentZ = startZ;
    while (fieldCounter < 4) {
      this.gameFieldGeometry(currentX, currentY, currentZ -= abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 8) {
      this.gameFieldGeometry(currentX -= abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 10) {
      this.gameFieldGeometry(currentX, currentY, currentZ -= abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 14) {
      this.gameFieldGeometry(currentX += abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 18) {
      this.gameFieldGeometry(currentX, currentY, currentZ -= abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 20) {
      this.gameFieldGeometry(currentX += abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 24) {
      this.gameFieldGeometry(currentX, currentY, currentZ += abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 28) {
      this.gameFieldGeometry(currentX += abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 30) {
      this.gameFieldGeometry(currentX, currentY, currentZ += abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 34) {
      this.gameFieldGeometry(currentX -= abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 38) {
      this.gameFieldGeometry(currentX, currentY, currentZ += abstand, fieldCounter.toString());
      fieldCounter++;
    }
    while (fieldCounter < 40) {
      this.gameFieldGeometry(currentX -= abstand, currentY, currentZ, fieldCounter.toString());
      fieldCounter++;
    }
    //STARTFELDER
    let startReferenceX;
    let StartReferenceZ;
    if (this.scene !== undefined) {
      let sceneObjX = this.scene.getObjectByName("field1");
      let sceneObjZ = this.scene.getObjectByName("field8");
      if (sceneObjX !== undefined) {
        startReferenceX = sceneObjX.position.x;
      }
      if (sceneObjZ !== undefined) {
        StartReferenceZ = sceneObjZ.position.z;
      }
    }

    if (startReferenceX !== undefined && StartReferenceZ !== undefined) {
      //STARTFELDER + PIN SCHWARZ
      this.gameFieldGeometry(startReferenceX - (3 * abstand), currentY, StartReferenceZ + (4 * abstand), "blackA", "black");
      //loadPin("black", [startReferenceX-(3*abstand), 1, StartReferenceZ+(4*abstand)], "spawn0Pin0");


      this.gameFieldGeometry(startReferenceX - (4 * abstand), currentY, StartReferenceZ + (4 * abstand), "blackB", "black");
      //loadPin("black", [startReferenceX-(4*abstand), 1, StartReferenceZ+(4*abstand)], "spawn0Pin1");

      this.gameFieldGeometry(startReferenceX - (4 * abstand), currentY, StartReferenceZ + (5 * abstand), "blackC", "black");
      //loadPin("black", [startReferenceX-(4*abstand), 1, StartReferenceZ+(5*abstand)], "spawn0Pin2");

      this.gameFieldGeometry(startReferenceX - (3 * abstand), currentY, StartReferenceZ + (5 * abstand), "blackD", "black");
      //loadPin("black", [startReferenceX-(3*abstand), 1, StartReferenceZ+(5*abstand)], "spawn0Pin3");

      //STARTFELDER + PIN Gelb
      this.gameFieldGeometry(startReferenceX - (3 * abstand), currentY, StartReferenceZ + (-4 * abstand), "yellowA", "yellow");
      //loadPin("yellow", [startReferenceX-(3*abstand), 1, StartReferenceZ+(-4*abstand)], "spawn1Pin0");

      this.gameFieldGeometry(startReferenceX - (4 * abstand), currentY, StartReferenceZ + (-4 * abstand), "yellowB", "yellow");
      //loadPin("yellow", [startReferenceX-(4*abstand), 1, StartReferenceZ+(-4*abstand)], "spawn1Pin1");

      this.gameFieldGeometry(startReferenceX - (4 * abstand), currentY, StartReferenceZ + (-5 * abstand), "yellowC", "yellow");
      //loadPin("yellow", [startReferenceX-(4*abstand), 1, StartReferenceZ+(-5*abstand)], "spawn1Pin2");

      this.gameFieldGeometry(startReferenceX - (3 * abstand), currentY, StartReferenceZ + (-5 * abstand), "yellowD", "yellow");
      //loadPin("yellow", [startReferenceX-(3*abstand), 1, StartReferenceZ+(-5*abstand)], "spawn1Pin3");

      //STARTFELDER + PIN  Rot
      this.gameFieldGeometry(startReferenceX - (-5 * abstand), currentY, StartReferenceZ + (4 * abstand), "redA", "red");
      //loadPin("red", [startReferenceX-(-5*abstand), 1, StartReferenceZ+(4*abstand)], "spawn2Pin0");

      this.gameFieldGeometry(startReferenceX - (-6 * abstand), currentY, StartReferenceZ + (4 * abstand), "redB", "red");
      //loadPin("red", [startReferenceX-(-6*abstand), 1, StartReferenceZ+(4*abstand)], "spawn2Pin1");

      this.gameFieldGeometry(startReferenceX - (-6 * abstand), currentY, StartReferenceZ + (5 * abstand), "redC", "red");
      //loadPin("red", [startReferenceX-(-6*abstand), 1, StartReferenceZ+(5*abstand)], "spawn2Pin2");

      this.gameFieldGeometry(startReferenceX - (-5 * abstand), currentY, StartReferenceZ + (5 * abstand), "redD", "red");
      //loadPin("red", [startReferenceX-(-5*abstand), 1, StartReferenceZ+(5*abstand)], "spawn2Pin3");

      //STARTFELDER + PIN  Gruen
      this.gameFieldGeometry(startReferenceX - (-5 * abstand), currentY, StartReferenceZ + (-4 * abstand), "greenA", "green");
      //loadPin("green", [startReferenceX-(-5*abstand), 1, StartReferenceZ+(-4*abstand)], "spawn3Pin0");

      this.gameFieldGeometry(startReferenceX - (-6 * abstand), currentY, StartReferenceZ + (-4 * abstand), "greenB", "green");
      //loadPin("green", [startReferenceX-(-6*abstand), 1, StartReferenceZ+(-4*abstand)], "spawn3Pin1");

      this.gameFieldGeometry(startReferenceX - (-6 * abstand), currentY, StartReferenceZ + (-5 * abstand), "greenC", "green");
      //loadPin("green", [startReferenceX-(-6*abstand), 1, StartReferenceZ+(-5*abstand)], "spawn3Pin2");

      this.gameFieldGeometry(startReferenceX - (-5 * abstand), currentY, StartReferenceZ + (-5 * abstand), "greenD", "green");
      //loadPin("green", [startReferenceX-(-5*abstand), 1, StartReferenceZ+(-5*abstand)], "spawn3Pin3");
    }


    //HÄUSCHENFELDER schwarz
    let n;
    let houseReferenceX;
    let houseReferenceZ;
    if (this.scene !== undefined) {
      let sceneObj = this.scene.getObjectByName("field38")
      if (sceneObj !== undefined) {
        houseReferenceX = sceneObj.position.x;
        houseReferenceZ = sceneObj.position.z;
      }
      if (houseReferenceX !== undefined && houseReferenceZ !== undefined) {
        for (n = 0; n < 4; n++) {
          this.gameFieldGeometry(houseReferenceX, currentY, houseReferenceZ - (n * abstand), "house0Pos" + n.toString(), "black");
        }
      }

      //HÄUSCHENFELDER gelb
      sceneObj = this.scene.getObjectByName("field8")
      if (sceneObj !== undefined) {
        houseReferenceX = sceneObj.position.x;
        houseReferenceZ = sceneObj.position.z;
      }
      if (houseReferenceX !== undefined && houseReferenceZ !== undefined) {
        for (n = 0; n < 4; n++) {
          this.gameFieldGeometry(houseReferenceX + (n * abstand), currentY, houseReferenceZ, "house1Pos" + n.toString(), "yellow");

        }
      }

      //HÄUSCHENFELDER grün
      sceneObj = this.scene.getObjectByName("field18")
      if (sceneObj !== undefined) {
        houseReferenceX = sceneObj.position.x;
        houseReferenceZ = sceneObj.position.z;
      }
      if (houseReferenceX !== undefined && houseReferenceZ !== undefined) {
        for (n = 0; n < 4; n++) {
          this.gameFieldGeometry(houseReferenceX, currentY, houseReferenceZ + (n * abstand), "house2Pos" + n.toString(), "green");

        }
      }

      //HÄUSCHENFELDER rot
      sceneObj = this.scene.getObjectByName("field28")
      if (sceneObj !== undefined) {
        houseReferenceX = sceneObj.position.x;
        houseReferenceZ = sceneObj.position.z;
      }
      if (houseReferenceX !== undefined && houseReferenceZ !== undefined) {
        for (n = 0; n < 4; n++) {
          this.gameFieldGeometry(houseReferenceX - (n * abstand), currentY, houseReferenceZ, "house3Pos" + n.toString(), "red");
        }
      }
    }
  }

  public whichColor(color: string) {
    switch (color) {
      case "red":
        color = "#FE2E2E";
        return color;
      case "green":
        color = "#008800";
        return color;
      case "yellow":
        color = "#F7FE2E";
        return color;
      case "black":
        color = "#0000FF";
        return color;
      case "white":
        color = "#FFFFFF";
        return color;
      default:
        return "#FFFFFF"
    }

  }

  public loadPins(color: string) {
    for (let a = 65; a <= 68; a++) {
      let position = [];
      let sceneObj = this.scene.getObjectByName("field" + color + String.fromCharCode(a));
      if (sceneObj !== undefined) {
        position[0] = sceneObj.position.x;
        position[1] = sceneObj.position.y + 2;
        position[2] = sceneObj.position.z;
      }

      this.createPin(color, position, color + "Pin" + String.fromCharCode(a));
    }


  }
/*
  public loadPin(color: string, position: number[], name: string) {
    let material = new THREE.MeshStandardMaterial({ color: this.whichColor(color) });
    let pin;
    let loaderPin = new GLTFLoader();
    loaderPin.load('../assets/pictures/texturen/PinV2.glb', (gltf: GLTF) => {
      pin = gltf.scene;

      pin.scale.set(3, 3, 3);
      pin.position.set(position[0], position[1], position[2]);
      //console.log(gltf.animations[0]);

      this.pinAnimationMixer = new THREE.AnimationMixer(pin);
      pin.traverse((o: any) => {
        if (o.isMesh) o.material = material;
        if (o.isMesh) o.name = name;
        if (o.isMesh) console.log(o.name);

      });
      this.pinAnimationClip = gltf.animations[0];
      pin.name = name;

      //pinArray.push(pin);
      this.scene.add(pin);
      //console.log(scene.getObjectByName(name));
      console.log("success");
    }, undefined, (error) => {
      console.error(error);
    });

  }*/

  public createPin(color: string, position: (number | undefined)[], name: string){

    let copy: Object3D | undefined;
    copy = this.loadedPinTest?.clone(true);

    // @ts-ignore
    copy.position.set(position[0], position[1], position[2]);
    copy.scale.set(3, 3, 3);

    let material = new THREE.MeshStandardMaterial({ color: this.whichColor(color) });
    this.pinAnimationMixer = new THREE.AnimationMixer(copy);
    copy.traverse((o: any) => {
      if (o.isMesh) o.material = material;
      if (o.isMesh) o.material = material;
      if (o.isMesh) o.name = name;
      if (o.isMesh) console.log(o.name);

    });
    copy.name = name;
    console.log(copy);

    this.scene.add(copy);
  }

  public movePinAt(pinName: string, toFieldName: string) {
    let positionX;
    let positionY;
    let positionZ;
    let sceneObj = this.scene.getObjectByName(toFieldName);
    if (sceneObj !== undefined) {
      positionX = sceneObj.position.x;
      positionY = sceneObj.position.y;
      positionZ = sceneObj.position.z;
    }
    sceneObj = this.scene.getObjectByName(pinName);
    if (sceneObj !== undefined) {
      if (positionX !== undefined && positionY !== undefined && positionZ !== undefined)
        sceneObj.position.set(positionX, positionY + 2, positionZ);
    }
  }

  public rollDice(number: number) {
    let loaderDice = new GLTFLoader();
    let dice;
    loaderDice.load('../assets/pictures/texturen/Dice_V1-' + number + '.glb', (gltf: GLTF) => {

      dice = gltf.scene;
      dice.scale.set(1, 1, 1);
      dice.position.set(0, 0, 50);
      dice.name = "dice";
      dice.children[3].visible = false;
      dice.children[2].visible = true;
      console.log(gltf);
      this.scene.add(dice);
      this.diceMixer = new THREE.AnimationMixer(dice);
      this.action = this.diceMixer.clipAction(gltf.animations[1]);
      this.action.loop = THREE.LoopOnce;
      this.action.clampWhenFinished = true;
      this.action.play();
      setTimeout(() => { this.scene.children.forEach(child => child.name == "dice" ? this.scene.remove(child) : null) }, 3000);
      console.log("success");
    }, undefined, (error) => {
      console.error(error);
    });
  }

  public movePintAtAnimation(pinName: string, toFieldName: string) {
    let positionX;
    let positionY;
    let positionZ;
    let sceneObj = this.scene.getObjectByName(toFieldName);
    if (sceneObj !== undefined) {
      positionX = sceneObj.position.x;
      positionY = sceneObj.position.y;
      positionZ = sceneObj.position.z;
    }
    let pin = this.scene.getObjectByName(pinName);
    //pinAnimationMixer=new THREE.AnimationMixer(pin);
    if (pin !== undefined && positionZ !== undefined && positionX !== undefined) {
      if (pin.position.x == positionX && pin.position.z < positionZ) {
        pin.rotation.y = -(Math.PI / 2);
      }
      if (pin.position.x == positionX && pin.position.z > positionZ) {
        pin.rotation.y = Math.PI / 2;
      }
      if (pin.position.x < positionX && pin.position.z == positionZ) {
        pin.rotation.y = -(Math.PI / 2);
      }
      if (pin.position.x > positionX && pin.position.z == positionZ) {
        pin.rotation.y = Math.PI / 2;
      }
    }
    console.log(this.pinAnimationClip);
    console.log(pin);
    let action;
    console.log("undifined?: " + this.pinAnimationMixer + ", " + this.pinAnimationClip);
    if (this.pinAnimationMixer !== undefined && this.pinAnimationClip !== undefined && pin !== undefined) {
      action = this.pinAnimationMixer.clipAction(this.pinAnimationClip, pin.children[0]);
    }
    console.log("undifined?: " + positionX + ", " + positionZ + ", " + pin + ", " + action);
    //action.loop=THREE.LoopOnce;
    if (positionX !== undefined && positionZ !== undefined && pin !== undefined && action !== undefined) {
      action.loop = THREE.LoopOnce;
      console.log("not undifined: " + positionX + ", " + positionZ + ", " + pin + ", " + action);
      this.tweenPin(positionX, positionZ, pin, action);
    }

    // scene.getChildByName(pinName).position.set(positionX,positionY+2,positionZ);

  }

  public tweenPin(positionX: number, positionZ: number, pin: Object3D, action: AnimationAction) {
    let target = { x: positionX, z: positionZ };
    let tween = new TWEEN.Tween(pin.position).to(target, 750);
    tween.start();
    console.log("tween.start: " + pin.position.x + ", " + pin.position.z + ", " + pin + ", " + action);
    tween.onComplete(() => console.log(pin.position));
    action.play();
  }

  public fieldPlayerOrder(playerName: string) {

  }
}
