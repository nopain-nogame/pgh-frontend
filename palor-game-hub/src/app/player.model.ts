export class Player {
    constructor(private playerName: string, private playerPermission: number, private playerReadyState: boolean) {}

    public getPlayerName() {
        return this.playerName;
    }

    public getPlayerPermission() {
        return this.playerPermission;
    }

    public getPlayerReadyState() {
        return this.playerReadyState;
    }

    public setPlayerName(newName: string) {
        this.playerName = newName;
    }

    public setPlayerPermission(newPermission: number) {
        this.playerPermission = newPermission;
    }

    public setPlayerReadyState(newReadyState: boolean) {
        this.playerReadyState = newReadyState;
    }
}
