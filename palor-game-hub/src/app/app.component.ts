import { routeTransitionAnimations } from './route-transition-animations';
import { map, catchError, tap } from 'rxjs/operators';
import { Component } from '@angular/core';
import { SocketService } from './socket.service';
import { NavigationEnd, NavigationStart, Router, RouterOutlet } from '@angular/router';

declare function setScreen(): void;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeTransitionAnimations]
})
export class AppComponent {
  title = 'Palor Game Hub';
  nickname = localStorage.getItem("nickname");

  public loggedIn: boolean = false;
  public gameMode: boolean = false;

  constructor(private socketService: SocketService, private router: Router) {
  }

  ngAfterViewInit(): void {
    this.socketService.connect();
    localStorage.setItem("permission", '2');
    localStorage.setItem("game", "4");
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.router.url !== '/home' && this.router.url !== '/join' && this.router.url !== '/create') {
          if (!this.loggedIn) {
            this.router.navigate(['/home']);
          } else {
            if (this.router.url === '/game') {
              this.gameMode = true;
            }
            if (this.router.url === '/select') {
              let currGame = localStorage.getItem("game");
              let gameID: number;
              if (currGame !== null) {
                gameID = + currGame;
                if (gameID !== 4) {
                  if (this.gameMode) {
                    this.router.navigate(['/game']);
                  } else {
                    this.router.navigate(['/lobby']);
                  }
                }
              }
            }
            if(this.router.url === '/lobby') {
              if(this.gameMode) {
                this.router.navigate(['/game']);
              }
            }
          }
        }
        if (this.router.url === '/join' || this.router.url === '/create') {
          let currPermission = localStorage.getItem("permission");
          let permission: number;
          if (currPermission !== null) {
            permission = + currPermission;
            if(permission !== 2) {
              let currGame = localStorage.getItem("game");
              let gameID: number;
              if (currGame !== null) {
                gameID = + currGame;
                if (gameID !== 4) {
                  if (this.gameMode) {
                    this.router.navigate(['/game']);
                  } else {
                    this.router.navigate(['/lobby']);
                  }
                } else {
                  this.router.navigate(['/select']);
                }
              }
            }
          }
        }
        setScreen();
      }
    });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet &&
      outlet.activatedRouteData &&
      outlet.activatedRouteData['animationState'];
  }

  loginData$ = this.socketService.messages$.subscribe((msg: any) => {
    if (msg.command == 'login') {
      this.loggedIn = true;
    }
    if (msg.command == 'createSession') {
      let sessName = localStorage.getItem("sessionName");
      if (sessName !== null) {
        this.title = sessName;
      }
    }
    if (msg.command == 'onSessionDataChanged') {
      this.title = msg.sessionName;
    }
  })
}
