import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-options',
  templateUrl: './game-options.component.html',
  styleUrls: ['./game-options.component.scss']
})
export class GameOptionsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public myPermission: number = this.getPermissionFromStorage();

  public isHost() {
    if(this.myPermission === 0) {
      return true;
    } else {
      return false;
    }
  }

  private getPermissionFromStorage() {
    let currPermission = localStorage.getItem("permission");
    let permission: number;
    if(currPermission !== null) {
      permission =+ currPermission;
      return permission;
    }
    permission = 1;
    return permission;
  }

}
