import { Router } from '@angular/router';
import { SocketService } from '../../socket.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/app/player.model';

declare function setScreen(): void;

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit {

  public players: Player[] = [];
  public myNickname: string = this.getNicknameFromStorage();
  public myPermission: number = this.getPermissionFromStorage();
  public myPlayer: Player = new Player(this.myNickname, this.myPermission, false);

  public setNewReadyState() {
    if(this.myPlayer.getPlayerReadyState()) {
      console.log("ready to notReady");
      this.myPlayer.setPlayerReadyState(false);
    } else {
      console.log("notReady to ready");
      this.myPlayer.setPlayerReadyState(true);
    }
    let currID = localStorage.getItem("sessionID");
    let sessID: number;
    if(currID !== null) {
      sessID = +currID;
      let msgObject: any = {
        "command": "setPlayerReadyState",
        "sessionID": sessID,
        "readyState": this.myPlayer.getPlayerReadyState()
      }
      this.socketService.sendMessage(msgObject);
    }
  }

  public isHost() {
    if(this.myPermission === 0) {
      return true;
    } else {
      return false;
    }
  }

  private getNicknameFromStorage() {
    let nickname = localStorage.getItem("nickname");
    if(nickname !== null) {
      return nickname;
    }
    return 'PalorPlayer';
  }

  private getPermissionFromStorage() {
    let currPermission = localStorage.getItem("permission");
    let permission: number;
    if(currPermission !== null) {
      permission =+ currPermission;
      return permission;
    }
    permission = 1;
    return permission;
  }

  sessionData$ = this.socketService.messages$.subscribe((msg: any) => {
    if(msg.command == 'setPlayerReadyState') {
      if(msg.status === false) {
        console.error(msg.reason);
      }
    }
    if(msg.command == 'onSessionDataChanged') {
      this.players = [];
      for(let player of msg.playerData) {
        if(this.myPlayer.getPlayerName() === player.playerName) {
          this.myPlayer = new Player(player.playerName, player.playerPermission, player.playerReadyState);
        } else {
          this.players.push(new Player(player.playerName, player.playerPermission, player.playerReadyState));
        }
      }
    }
    if(msg.command == 'gameStart') {
      this.router.navigate(['/game']);
    }
  })

  public callSetScreen(): void {
    setScreen();
  }

  constructor(private socketService: SocketService, private router: Router) { }

  ngOnInit(): void {
  }
}


