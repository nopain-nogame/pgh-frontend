import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-session-form',
  templateUrl: './session-form.component.html',
  styleUrls: ['./session-form.component.scss']
})
export class SessionFormComponent implements OnInit {

  public sessionID = localStorage.getItem("sessionID");
  public sessionPasswort = localStorage.getItem("sessionPasswort");

  constructor() {
  }

  ngOnInit(): void {
  }

  public myPermission: number = this.getPermissionFromStorage();

  public isHost() {
    if(this.myPermission === 0) {
      return true;
    } else {
      return false;
    }
  }

  private getPermissionFromStorage() {
    let currPermission = localStorage.getItem("permission");
    let permission: number;
    if(currPermission !== null) {
      permission =+ currPermission;
      return permission;
    }
    permission = 1;
    return permission;
  }

}
