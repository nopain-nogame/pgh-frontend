import { OnCreate } from './on-create.directive';
import { PlayerListComponent } from './lobby/player-list/player-list.component';
import { SocketService } from './socket.service';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { BannerComponent } from './banner/banner.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import {ThumbnailComponent} from './game-select/thumbnail/thumbnail.component';
import { GameDetailComponent } from './game-select/game-detail/game-detail.component';
import { GameSelectComponent } from './game-select/game-select.component';
import { CreateFormComponent } from './login-form/create-form/create-form.component';
import { JoinFormComponent } from './login-form/join-form/join-form.component';
import { LobbyComponent } from './lobby/lobby.component';
import { SessionFormComponent } from './lobby/session-form/session-form.component';
import { GameOptionsComponent } from './lobby/game-options/game-options.component';
import { GameComponent } from './game/game.component';
import { HudComponent } from './game/hud/hud.component';

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    LoginFormComponent,
    ThumbnailComponent,
    GameDetailComponent,
    GameSelectComponent,
    CreateFormComponent,
    JoinFormComponent,
    LobbyComponent,
    SessionFormComponent,
    PlayerListComponent,
    GameOptionsComponent,
    OnCreate,
    GameComponent,
    HudComponent
  ],
  imports: [
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxGalleryModule
  ],
  providers: [SocketService],
  bootstrap: [AppComponent]
})
export class AppModule {}
