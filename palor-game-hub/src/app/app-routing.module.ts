import { GameComponent } from './game/game.component';
import { LobbyComponent } from './lobby/lobby.component';
import { GameSelectComponent } from './game-select/game-select.component';
import { JoinFormComponent } from './login-form/join-form/join-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { CreateFormComponent } from './login-form/create-form/create-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'home', component: LoginFormComponent, data: { animationState: 'One' }},
  { path: 'create', component: CreateFormComponent, data: { animationState: 'Two' }},
  { path: 'join', component: JoinFormComponent, data: { animationState: 'Two' }},
  { path: 'select', component: GameSelectComponent, data: { animationState: 'Three' }},
  { path: 'lobby', component: LobbyComponent, data: { animationState: 'Four' }},
  { path: 'game', component: GameComponent, data: { animationState: 'Five' }},
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
