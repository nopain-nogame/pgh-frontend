import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from 'src/app/socket.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {

  public nicknameStorage = localStorage.getItem("nickname");
  public sessionStorage = localStorage.getItem("sessionName");
  public sessionExists = false;
  public errColor = '#A62C21';

  onCreateSubmit(value: any){
    this.getLogin();
    this.setNickname(value.nickname);
    this.setSessionName(value.sessionName);
    let msgObject: any = {
      "command": "createSession",
	    "sessionName": value.sessionName,
	    "username": value.nickname
    }
    this.socketService.sendMessage(msgObject);
  }
  

  sessionData$ = this.socketService.messages$.subscribe((msg: any) => {
    if(msg.command == 'createSession') {
      if(msg.status === true) {
        localStorage.setItem("sessionID", msg.sessionID);
        localStorage.setItem("sessionPasswort", msg.sessionPassword);
        localStorage.setItem("permission",'0');
        this.sessionExists = false;
        this.router.navigate(['/select']);
      } else {
        console.log("false");
        this.sessionExists = true;
        this.socketService.close();
        this.socketService.connect();
      }
    }
  })

  constructor(private socketService: SocketService, private router: Router) {
  }

  private setNickname(value: string): void {
    localStorage.setItem("nickname", value);
    this.nicknameStorage = value;
  }

  private setSessionName(value: string): void {
    localStorage.setItem("sessionName", value);
    this.sessionStorage = value;
  }

  ngOnInit(): void {
  }

  getLogin() {
    let msgObject: any = {
      "command": "login"
    }
    this.socketService.sendMessage(msgObject);
  }

}
