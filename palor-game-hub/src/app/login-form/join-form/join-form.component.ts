import { Router } from '@angular/router';
import { SocketService } from './../../socket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-join-form',
  templateUrl: './join-form.component.html',
  styleUrls: ['./join-form.component.scss']
})
export class JoinFormComponent implements OnInit {

  public nicknameStorage = localStorage.getItem("nickname");
  public errMessage = '';

  constructor(private socketService: SocketService, private router: Router) { }

  onLoginSubmit(value: any) {
    this.getLogin();
    this.setNickname(value.nickname);
    let id: number = +value.sessionID;
    localStorage.setItem("sessionID", value.sessionID);
    localStorage.setItem("sessionPasswort", value.sessionPw);
    let msgObject: any = {
      "command": "joinSession",
      "username": value.nickname,
      "sessionID": id,
      "sessionPassword": value.sessionPw
    }
    this.socketService.sendMessage(msgObject);
  }

  sessionData$ = this.socketService.messages$.subscribe((msg: any) => {
    if (msg.command == 'joinSession') {
      if (msg.status === true) {
        console.log("Session mit ID:" + msg.sessionID + " beigetreten");
        localStorage.setItem("permission",'1');
        this.errMessage = '';
        this.router.navigate(['/lobby']);
      } else {
        console.log(msg.reason);
        this.errMessage = msg.reason;
        this.socketService.close();
        this.socketService.connect();
      }
    }
  })

  private setNickname(value: string): void {
    localStorage.setItem("nickname", value);
    this.nicknameStorage = value;
  }

  ngOnInit(): void {
  }

  getLogin() {
    let msgObject: any = {
      "command": "login"
    }
    this.socketService.sendMessage(msgObject);
  }

}
