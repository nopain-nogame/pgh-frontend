import { Component, EventEmitter, Input, OnChanges, OnInit, Optional, Output, SimpleChange, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

  public detailObjects: Detail[] = [];

  public _index: number = 0;

  get index(): number {
    return this._index;
  }

  @Input()
  set index(val: number) {
    this._index = val;
  }

  mockObject = {
    "title": "Mensch ärgere dich nicht",
    "desc": "Das beliebte Original Mensch ärgere Dich nicht von Schmidt Spiele hat bereits Generationen Jung und Alt zum Spielen miteinander gebracht. Und was den einen ärgert, freut den anderen. Dieser gelungene Mix macht das pfiffige Konzept aus kinderleichter Spielregel und unwägbarem Würfelglück seit eh und je zu einem geschätzten Wegbegleiter. Mal zum Ärgern, mal zum Freuen.",
    "minPlayer": 2,
    "maxPlayer": 4,
    "bannerLink": "../../assets/pictures/maedn_schriftzug.png"
  }
  mockObject2 = {
    "title": "Coming Soon",
    "desc": "Coming Soon",
    "minPlayer": 3,
    "maxPlayer": 4
  }
  mockObject3 = {
    "title": "Coming Soon",
    "desc": "Coming Soon",
    "minPlayer": 1,
    "maxPlayer": 3
  }

  constructor() { }

  ngOnInit(): void {
    this.detailObjects.push(new Detail(this.mockObject.title, this.mockObject.desc, this.mockObject.minPlayer, this.mockObject.maxPlayer, this.mockObject.bannerLink));
    this.detailObjects.push(new Detail(this.mockObject2.title, this.mockObject2.desc, this.mockObject2.minPlayer, this.mockObject2.maxPlayer, ''));
    this.detailObjects.push(new Detail(this.mockObject3.title, this.mockObject3.desc, this.mockObject3.minPlayer, this.mockObject3.maxPlayer, ''));
  }

}

class Detail {
  constructor(private title: string, private desc: string, private minPlayer: number, private maxPlayer: number, private bannerLink: string) { }

  public getLink() {
    return this.bannerLink;
  }

  public getTitle() {
    return this.title;
  }

  public getDesc() {
    return this.desc;
  }

  public getPlayerCount() {
    return this.minPlayer + "-" + this.maxPlayer;
  }
}