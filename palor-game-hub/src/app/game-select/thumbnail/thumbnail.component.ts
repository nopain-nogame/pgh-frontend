import { GameSelectComponent } from './../game-select.component';
import { Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from '@kolkov/ngx-gallery';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';



@Component({
  selector: 'app-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})



export class ThumbnailComponent implements OnInit {
  galleryOptions!: NgxGalleryOptions[];
  galleryImages!: NgxGalleryImage[];



  constructor() {

  }

  @Output("getIndex") getIndex: EventEmitter<any> = new EventEmitter();
  @Output("setIndex") setIndex: EventEmitter<number> = new EventEmitter();
  @Output("getRules") getRules: EventEmitter<any> = new EventEmitter();



  ngOnInit() {


    this.galleryOptions = [
      {
        width: "800px",
        height: "500px",
        thumbnailsColumns: 3,
        thumbnailsRows: 1,
        thumbnailsArrows: false,
        imageArrows: true,
        preview: false,
        thumbnails: true,
        thumbnailsRemainingCount: true,
        thumbnailsSwipe: true,
        arrowNextIcon: 'fa fa-chevron-right',
        arrowPrevIcon: 'fa fa-chevron-left',
        imageInfinityMove: true,
        imageAutoPlay: false,
        imageAutoPlayInterval: 3000,
        imageAnimation: NgxGalleryAnimation.Fade
      },
      // max-width 940
      {
        breakpoint: 940,
        height: "475px",
      },
      // max-width 625
      {
        breakpoint: 625,
        height: "350px",
      },

      // max-width 410
      {
        breakpoint: 410,
        height: "250px",
      }
    ];

    this.galleryImages = [
      {
        small: 'assets/pictures/games-icon/MÄDN/game_1.png',
        medium: 'assets/pictures/games-icon/MÄDN/game_1.png',
        big: 'assets/pictures/games-icon/MÄDN/game_1.png'
      },
      {
        small: 'assets/pictures/games-icon/game_2/game_2.png',
        medium: 'assets/pictures/games-icon/game_2/game_2.png',
        big: 'assets/pictures/games-icon/game_2/game_2.png'
      },
      {
        small: 'assets/pictures/games-icon/game_3/game_3.png',
        medium: 'assets/pictures/games-icon/game_3/game_3.png',
        big: 'assets/pictures/games-icon/game_3/game_3.png'
      },
    ];
  }
}