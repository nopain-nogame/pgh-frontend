import { SocketService } from './../socket.service';
import { logging } from 'protractor';
import { AfterContentChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-select',
  templateUrl: './game-select.component.html',
  styleUrls: ['./game-select.component.scss']
})
export class GameSelectComponent implements OnInit {
  public index: number = 0;
  private ruleNumber: number = 0;
  _ruleList!: rules[];

  constructor(private socketService: SocketService, private router: Router) { }

  public setIndex(index: number): void {
    this.index = index;
    this.ruleNumber = index;
  }

  public getIndex() {
    return this.index;
  }

  public chooseGame(): void {
    let currID = localStorage.getItem("sessionID");
    let sessID: number;
    if(currID !== null) {
      sessID = +currID;
      let msgObject: any = {
        "command": "setSessionGame",
        "sessionID": sessID,
        "sessionGame": this.index
      }
      this.socketService.sendMessage(msgObject);
    }
  }

  sessionData$ = this.socketService.messages$.subscribe((msg: any) => {
    if(msg.command == 'setSessionGame') {
      if(msg.status === true) {
        console.log(msg.reason);
        this.router.navigate(['/lobby']);
        localStorage.setItem("game", "" + this.index);
      } else {
        console.error(msg.reason);
        this.router.navigate(['/create']);
      }
    }
  })

  public getRules() {
   
    switch (this.ruleNumber) {
      case 0:  console.log("Get Rules Funktioniert du arsch 1"); this._ruleList = [
        { id: 1, name: "Option 1 für MÄDN" },
        { id: 2, name: "Option 2 für MÄDN" },
        { id: 3, name: "Option 3 für MÄDN" },
        { id: 4, name: "Option 4 für MÄDN" }
      ];
        break;
      case 1:  console.log("Get Rules Funktioniert du arsch 2"); this._ruleList = [
        { id: 1, name: "Option 1 für Spiel 2" },
        { id: 2, name: "Option 2 für Spiel 2" },
        { id: 3, name: "Option 3 für Spiel 2" },
        { id: 4, name: "Option 4 für Spiel 2" }
      ];
        break;
      case 2:  console.log("Get Rules Funktioniert du arsch 3"); this._ruleList = [
        { id: 1, name: "Option 1 für Spiel 3" },
        { id: 2, name: "Option 2 für Spiel 3" },
        { id: 3, name: "Option 3 für Spiel 3" },
        { id: 4, name: "Option 4 für Spiel 3" }
      ];
        break;
      default: console.log("Keine Regeln für das gewählte Spiel");
        break;
    }
  }



  ngOnInit(): void {
  }

}

class rules {
  id!: number;
  name!: String;
}
